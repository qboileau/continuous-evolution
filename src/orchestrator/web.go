package orchestrator

import (
	"continuous-evolution/src/downloader"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"continuous-evolution/src/project"

	"github.com/sirupsen/logrus"
)

type message struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	AddProject  string `json:"addProject,omitempty"`
}

type exclude struct {
	Type  project.PackageType `json:"type"`
	Value string              `json:"value"`
}

//WebConfig is configuration required by web
type WebConfig struct {
	Enabled bool
	Port    int
}

//DefaultWebConfig is the default configuration required by web
var DefaultWebConfig = WebConfig{Enabled: true, Port: 1234}

var loggerWeb = logrus.WithField("logger", "web")

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	data, _ := json.Marshal(message{
		Title:       "Continuous Evolution",
		Description: "Keep the world updated!",
		AddProject:  fmt.Sprintf("%s/%s", r.Host, project.WebURLAddProject),
	})
	io.WriteString(w, string(data))
}

func handlerAddProject(sender Sender, downloaderManager downloader.Manager) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		query := r.URL.Query()

		p, err := downloaderManager.BuildProject(query.Get("project"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			data, _ := json.Marshal(message{
				Title:       "Bad url",
				Description: err.Error(),
			})
			io.WriteString(w, string(data))
			return
		}

		if query.Get("project") != "" {
			if r.Method == "POST" {
				decoder := json.NewDecoder(r.Body)
				var excludes map[string][]string
				err := decoder.Decode(&excludes)
				if err != nil {
					loggerWeb.WithField("query", query).Error(err)
				}
				defer r.Body.Close()
				loggerWeb.WithField("params", excludes).Info("exclusion detected")
				p = p.SetExcludes(excludes)
				loggerWeb.WithField("excludes", excludes).Info("exclusion list defined")
			}

			sender.Send(p)

			data, _ := json.Marshal(message{
				Title:       "Project in progress",
				Description: "You will receive a merge request soon as possible.",
			})
			io.WriteString(w, string(data))
		} else {
			data, _ := json.Marshal(message{
				Title:       "Add new project",
				Description: fmt.Sprintf("Add query %s/%s?project=gitUrl then wait to receive (merge | pull)-request ;)", r.Host, project.WebURLAddProject),
				AddProject:  fmt.Sprintf("%s/%s", r.Host, project.WebURLAddProject),
			})
			io.WriteString(w, string(data))
		}
	}
}

func handlerMergeRequest(sender Sender, downloaderManager downloader.Manager) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		query := r.URL.Query()

		p, err := downloaderManager.BuildProject(query.Get("project"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			data, _ := json.Marshal(message{
				Title:       "Bad url",
				Description: err.Error(),
			})
			io.WriteString(w, string(data))
			return
		}

		if query.Get("project") != "" {
			if r.Method == "POST" {
				decoder := json.NewDecoder(r.Body)
				var excludes map[string][]string
				err := decoder.Decode(&excludes)
				if err != nil {
					loggerWeb.WithField("query", query).Error(err)
				}
				defer r.Body.Close()
				loggerWeb.WithField("params", excludes).Info("exclusion detected")
				p = p.SetExcludes(excludes)
				loggerWeb.WithField("excludes", excludes).Info("exclusion list defined")
			}

			sender.Send(p)

			data, _ := json.Marshal(message{
				Title:       "Project in progress",
				Description: "You will receive a merge request soon as possible.",
			})
			io.WriteString(w, string(data))
		} else {
			data, _ := json.Marshal(message{
				Title:       "Add new project",
				Description: fmt.Sprintf("Add query %s/%s?project=gitUrl then wait to receive (merge | pull)-request ;)", r.Host, project.WebURLAddProject),
				AddProject:  fmt.Sprintf("%s/%s", r.Host, project.WebURLAddProject),
			})
			io.WriteString(w, string(data))
		}
	}
}

//StartWeb listen on port set in configuration
func startWeb(sender Sender, config WebConfig, downloaderManager downloader.Manager) {
	defer sender.Close()
	http.HandleFunc("/", handler)
	http.HandleFunc(fmt.Sprintf("/%s", project.WebURLAddProject), handlerAddProject(sender, downloaderManager))
	http.HandleFunc(fmt.Sprintf("/%s", project.WebURLMergeRequest), handlerMergeRequest(sender, downloaderManager))
	loggerWeb.WithField("port", config.Port).Info("Server web is started")
	err := http.ListenAndServe(":"+strconv.Itoa(config.Port), nil)
	if err != nil {
		loggerWeb.WithError(err).Error("Error listenAndServe in web server")
	}
}
