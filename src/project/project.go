package project

import (
	"continuous-evolution/src/project/io"
	"strings"
	"time"
)

//Project represent the main model of applications.
type Project struct {
	CreatedDate        time.Time           `json:"createdDate"`
	LastUpdatedDate    time.Time           `json:"LastUpdatedDate"`
	Name               string              `json:"name"`
	GitURL             string              `json:"gitUrl"`
	BranchName         string              `json:"branchName"`
	PathToWrite        string              `json:"pathToWrite"`
	Packages           []Package           `json:"packages"`
	Login              string              `json:"login"`
	Token              string              `json:"token"`
	TypeHost           string              `json:"typeHost"`
	Organisation       string              `json:"organisation"`
	BranchAlreadyExist bool                `json:"branchAlreadyExist"`
	Excludes           map[string][]string `json:"excludes"`
	ReProcessDistantID string              `json:"reProcessDistantId"` //id of merge-request or pull-request
	URLReProcess       string              `json:"urlReProcess"`
}

//HTTP return an instance of io.http to call http with project
func (p Project) HTTP() *io.HTTP {
	//TODO when #90 is done, move this code into NewProject()
	return io.NewHTTP(p.Login, p.Token)
}

//Git return an instance of io.http to call http with project
func (p Project) Git() *io.Git {
	//TODO when #90 is done, move this code into NewProject()
	return io.NewGit(p.PathToWrite, p.Name, p.GitURL, p.BranchAlreadyExist, p.BranchName)
}

//Copy make a clone of current project
func (p Project) Copy() *Project {
	return &Project{
		CreatedDate:        p.CreatedDate,
		LastUpdatedDate:    p.LastUpdatedDate,
		Name:               p.Name,
		GitURL:             p.GitURL,
		BranchName:         p.BranchName,
		PathToWrite:        p.PathToWrite,
		Packages:           p.Packages,
		Login:              p.Login,
		Token:              p.Token,
		TypeHost:           p.TypeHost,
		Organisation:       p.Organisation,
		Excludes:           p.Excludes,
		BranchAlreadyExist: p.BranchAlreadyExist,
		ReProcessDistantID: p.ReProcessDistantID,
		URLReProcess:       p.URLReProcess,
	}
}

//SetGitURL set git url to clone project.
//Following immutability principe this function return a new project.
func (p Project) SetGitURL(GitURL string) Project {
	copy := p.Copy()
	copy.GitURL = GitURL
	return *copy
}

//SetURLReProcess set a url to relaunch process.
//Following immutability principe this function return a new project.
func (p Project) SetURLReProcess(urlReProcess string) Project {
	copy := p.Copy()
	copy.URLReProcess = urlReProcess
	return *copy
}

//SetReProcessDistantID set an id of merge-request or pull-request to relaunch process.
//Following immutability principe this function return a new project.
func (p Project) SetReProcessDistantID(reProcessDistantID string) Project {
	copy := p.Copy()
	copy.ReProcessDistantID = reProcessDistantID
	return *copy
}

//SetBranchAlreadyExist permit to update update a project when a merge request is already starting.
//Following immutability principe this function return a new project.
func (p Project) SetBranchAlreadyExist(b bool) Project {
	copy := p.Copy()
	copy.BranchAlreadyExist = b
	return *copy
}

//AddPackages permit to add new packages to a project.
//Following immutability principe this function return a new project.
func (p Project) AddPackages(packages []Package) *Project {
	copy := p.Copy()
	copy.Packages = append(p.Packages, packages...)
	return copy
}

//SetExcludes set excludes to a copy project and return it
func (p Project) SetExcludes(excludes map[string][]string) Project {
	copy := p.Copy()
	copy.Excludes = excludes
	return *copy
}

//UpdateLastUpdatedDate set time.Now() in LastUpdatedDate to a copy and return it
func (p Project) UpdateLastUpdatedDate() Project {
	copy := p.Copy()
	copy.LastUpdatedDate = time.Now()
	return *copy
}

//IsSame compare project by typeHost, organisation and name
func (p Project) IsSame(typeHost string, organisation string, name string) bool {
	return p.TypeHost == typeHost && p.Organisation == organisation && p.Name == name
}

//PackageType is a string representing a kind of package like npm, maven, pip, docker...
type PackageType string

//Package is a PackageType and a path to find main file line package.json, pom.xml... THe path can be a directory or a file depending on the package.
type Package struct {
	Type                PackageType  `json:"type"`
	Path                string       `json:"relativePath"`
	UpdatedDependencies []Dependency `json:"updatedDependencies"`
}

//AddUpdatedDependencies add updated dependencies in package.
//Following immutability principe this function return a new project.
func (p Package) AddUpdatedDependencies(dependencies []Dependency) Package {
	return Package{
		Type:                p.Type,
		Path:                p.Path,
		UpdatedDependencies: append(p.UpdatedDependencies, dependencies...),
	}
}

//CleanPath return path to the pkg without /tmp/ dir.
//Used in template of MergeRequest
func (p Package) CleanPath(project Project) string {
	return strings.TrimPrefix(p.Path, project.PathToWrite+"/"+project.Name+"/")
}

//IsExcludes return true when the pkg is excluded (and no dependencies are excluded)
//Used in template of MergeRequest
func (p Package) IsExcludes(project Project) bool {
	values, ok := project.Excludes[p.CleanPath(project)]
	return ok && len(values) == 0
}

//Dependency is a dependency including in a Package
type Dependency struct {
	Name            string `json:"name"`
	NewVersion      string `json:"newVersion"`
	OriginalVersion string `json:"originalVersion"`
	CanBeExcludes   bool   `json:"canBeExcludes"`
}

//IsExcludes return true when the dependency is excluded (and the pkg is not)
//Used in template of MergeRequest
func (d Dependency) IsExcludes(pkg Package, project Project) bool {
	values, ok := project.Excludes[pkg.CleanPath(project)]
	if ok && len(values) > 0 {
		for _, dep := range values {
			if d.Name == dep {
				return true
			}
		}
	}
	return false
}
