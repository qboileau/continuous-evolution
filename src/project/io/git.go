package io

import (
	"fmt"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

const (
	commitUpdate = "New dependencies"
	commitReset  = "Reset from master"
)

var loggerGit = logrus.WithField("logger", "project/io/git")

//Git is the struct to exec cmd git with project
type Git struct {
	pathToWrite        string
	name               string
	gitURL             string
	branchAlreadyExist bool
	branchName         string
}

//NewGit return an instance of Git struct
func NewGit(pathToWrite string, name string, gitURL string, branchAlreadyExist bool, branchName string) *Git {
	return &Git{pathToWrite, name, gitURL, branchAlreadyExist, branchName}
}

//Clone git clone the project to `project.PathToWrite`
//If merge request already exist clone only the branch `project.BranchName`
//Else do regular clone
func (g *Git) Clone() error {
	//git clone can be called 2 times with branchAlreadyExist()
	if !PathExists(fmt.Sprintf("%s/%s", g.pathToWrite, g.name)) {
		cmd := exec.Command("git", "clone", g.gitURL)
		cmd.Dir = g.pathToWrite
		output, err := cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithField("cmd", "git clone "+g.gitURL).WithError(err).Error("Error making git clone")
			return err
		}
	}

	//get all source code from master
	//useful if user restart process with exclude depth
	//if we don't do that exclude will not be excluded because there are already updated
	if g.branchAlreadyExist {
		defaultBranchName, err := g.DefaultBranch()
		if err != nil {
			return err
		}

		cmdDir := g.pathToWrite + "/" + g.name

		//git fetch origin UpdateLibs
		cmd := exec.Command("git", "fetch", "origin", g.branchName)
		cmd.Dir = cmdDir
		output, err := cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithError(err).Error("Error making git fetch")
			return err
		}
		//git checkout FETCH_HEAD
		cmd = exec.Command("git", "checkout", "FETCH_HEAD")
		cmd.Dir = cmdDir
		output, err = cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithError(err).Error("Error making git checkout FETCH_HEAD")
			return err
		}
		//git checkout -b UpdateLibs
		cmd = exec.Command("git", "checkout", "-b", g.branchName)
		cmd.Dir = cmdDir
		output, err = cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithError(err).Error("Error making git checkout -b branch")
			return err
		}
		//git checkout $defaultBranch -- .
		cmd = exec.Command("git", "checkout", defaultBranchName, "--", ".")
		cmd.Dir = cmdDir
		output, err = cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithError(err).Error("Error making git checkout branch -- .")
			return err
		}
		//git commit -am "reset"
		cmd = exec.Command("git", "commit", "-am", commitReset)
		cmd.Dir = cmdDir
		output, err = cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithError(err).Error("Error making git commit -am")
			return err
		}
		//git rebase defaultBranchName -X theirs to rebase code but keep only theirs modifications to avoid conflict
		cmd = exec.Command("git", "rebase", defaultBranchName, "-X", "theirs")
		cmd.Dir = cmdDir
		output, err = cmd.Output()
		if err != nil {
			loggerGit.WithField("output", string(output)).WithError(err).Error("Error making git rebase " + defaultBranchName + " -X theirs")
			return err
		}
	}

	return nil
}

//BranchAlreadyExist test if distant remote has `project.BranchName` branch
func (g *Git) BranchAlreadyExist() (bool, error) {
	//if path don't exist first git clone and then check distant branch
	if !PathExists(fmt.Sprintf("%s/%s", g.pathToWrite, g.name)) {
		err := g.Clone()
		if err != nil {
			return false, err
		}
	}

	cmd := exec.Command("git", "ls-remote", "--heads", "origin", g.branchName)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	branchAlreadyExist := false
	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithError(err).WithField("output", string(output)).Error("Can't ls-remote")
		return branchAlreadyExist, err
	} else if strings.Replace(string(output), "\n", "", -1) != "" {
		branchAlreadyExist = true
	}
	return branchAlreadyExist, nil
}

//DefaultBranch return the name of the branch where HEAD is
func (g *Git) DefaultBranch() (string, error) {
	cmd := exec.Command("git", "rev-parse", "--abbrev-ref", "origin/HEAD")
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	output, err := cmd.CombinedOutput()
	if err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't get default branch")
		return "", err
	}
	defaultBranch := strings.Replace(string(output), "\n", "", -1)
	names := strings.Split(defaultBranch, "/")
	if len(names) != 2 {
		return "", fmt.Errorf("default branch can't be determined, from %s", defaultBranch)
	}
	return names[1], nil
}

//Diff return true if git workdir is not clean e.g. a modif is not committed
func (g *Git) Diff() bool {
	cmd := exec.Command("git", "diff", "--exit-code")
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	return cmd.Run() != nil
}

//NewBranch create a new branch named `project.BranchName`
func (g *Git) NewBranch() error {
	cmd := exec.Command("git", "checkout", "-b", g.branchName)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)

	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", output).WithError(err).Error("Can't create branch")
		return err
	}

	return nil
}

//Commit commit current code
func (g *Git) Commit() error {
	cmd := exec.Command("git", "commit", "-am", commitUpdate)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)

	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't commit")
		return err
	}

	return nil
}

//PushForce push current modifications
func (g *Git) PushForce() error {
	cmd := exec.Command("git", "push", "-f", "origin", g.branchName)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)

	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't push")
		return err
	}

	return nil
}

//TrackFile return true if its a file tracked by git, false otherwise
func (g *Git) TrackFile(path string) (bool, error) {
	cmd := exec.Command("git", "ls-files", path)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)

	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't ls-files")
		return false, err
	} else if len(output) == 0 {
		return false, nil
	}
	return true, nil
}
