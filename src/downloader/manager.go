package downloader

import (
	"continuous-evolution/src/project"
	"errors"
)

type downloader interface {
	accept(string) bool
	buildProject(string) (project.Project, error)
	download(project.Project) (project.Project, error)
}

//Config is configuration required by Downloader
type Config struct {
	PoolSize     int
	PathToWrite  string
	BranchName   string
	Local        LocalConfig
	Git          GitConfig
	Pullrequest  PullRequestConfig
	Mergerequest MergeRequestConfig
}

//DefaultConfig is the default configuration required by downloader
var DefaultConfig = Config{PoolSize: 1, PathToWrite: "/tmp", BranchName: "ConEvolUpdateLibs", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}

type manager struct {
	downloaders []downloader
}

//Manager is reposible to download the code
type Manager interface {
	BuildProject(url string) (project.Project, error)
	Download(project project.Project) (project.Project, error)
}

//DeleteProject must delete the project when called
type DeleteProject interface {
	Delete(project project.Project) error
}

//NewManager return a Manager configured
func NewManager(config Config, deleter DeleteProject) Manager {
	downloaders := make([]downloader, 0)
	if config.Pullrequest.Enabled {
		downloaders = append(downloaders, newPullRequest(config, deleter)) // newPullRequest() must be before newGit() because it's more precise
	}
	if config.Mergerequest.Enabled {
		downloaders = append(downloaders, newMergeRequest(config, deleter)) // newMergeRequest() must be before newGit() because it's more precise
	}
	if config.Git.Enabled {
		downloaders = append(downloaders, newGit(config))
	}
	if config.Local.Enabled {
		downloaders = append(downloaders, newLocal(config))
	}
	return &manager{
		downloaders: downloaders,
	}
}

//BuildProject return project if at least one downloader will accept this url
func (m *manager) BuildProject(url string) (project.Project, error) {
	for _, downloader := range m.downloaders {
		if downloader.accept(url) {
			return downloader.buildProject(url)
		}
	}
	return project.Project{}, errors.New("your git url must be a valid git path or conform to `https://([^:]+):([^@]+)@([^/]+)/([^/]+)/([^\\.]+)\\.git` where 1=login 2=token 3=github.com|gitlab url 4=organisation 5=projectName")
}

//Download use downloaders to download code
func (m *manager) Download(project project.Project) (project.Project, error) {
	for _, downloader := range m.downloaders {
		if downloader.accept(project.GitURL) {
			return downloader.download(project)
		}
	}
	return project, errors.New("No downloader found for this project")
}
