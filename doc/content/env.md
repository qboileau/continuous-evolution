---
title: "Env"
menu: 
    main:
        weight: 4
        parent: "Develop"
subnav: "true"
description: "How to develop ?"
---
# Develop

## Development with simple cli

```bash
cd $GOPATH/src
git clone git@gitlab.com:ContinuousEvolution/continuous-evolution.git
cd continuous-evolution
make
./build/cli -url https://$PSEUDO:$PRIVATE_TOKEN@github.com/$ORGANISATION/PROJECT.git
```

If you prefer work with local project use :

```bash
./build/cli -url /absolute/url/to/my/project/
```

> Look logs to see results and if all is ok you have a fresh pullrequest with new dependencies.
